FROM alpine:3.10

RUN echo "**** install Python ****" && \
    apk add --no-cache  python3-dev libevent-dev libffi-dev libressl-dev libstdc++  && \
    apk add --no-cache g++ && \
    if [ ! -e /usr/bin/python ]; then ln -sf python3 /usr/bin/python ; fi && \
    \
    echo "**** install pip ****" && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --no-cache --upgrade pip setuptools wheel && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi


ADD __init__.py /

COPY codigo/ ./opt/app
WORKDIR /opt/app


# docker, local
ENV SLACK_API_TOKEN=xoxp-716214992339-727194296372-727820679904-fd6715439b049d524642145093dee41b
ENV SLACK_CHANNEL=#si
ENV BACKEND_URL=https://10.3.1.100:443
ENV APP_ID=admin
ENV PSW_ID=redhat
ENV INIT_RUN=run


RUN cd /opt/app &&  pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "python", "./__init__.py" ]
