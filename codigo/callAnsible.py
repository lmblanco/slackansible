import os
from codigo.ClientHttpRequest import post
from codigo.ClientHttpRequest import get


urlAnsible=os.environ['BACKEND_URL']
idAnsible=os.environ['APP_ID']
pwdAnsible=os.environ['PSW_ID']

def ejecutarJob(tarea):
    auth = (idAnsible, pwdAnsible)
    jobTemplate = urlAnsible + '/api/v2/job_templates/'
    r = get(jobTemplate, auth)
    menssage = 'Error [' + tarea + ']'
    tareaBus = tarea.lower()
    if r.status_code == 200 or r.status_code == 201:
        jsonRes = r.json()
        url=''
        for item in jsonRes["results"]:
            #print(tareaBus)
            #print(item['name'].lower())
            if tareaBus == item['name'].lower():
                url = item['url']
                break
        if url != '':
            host_config_key=''
            jobTemplate = urlAnsible+url+'callback/'
            r = get(jobTemplate,auth)
            jsonRes = r.json()
            host_config_key = {'host_config_key': jsonRes['host_config_key']}

            #print(host_config_key)
            jobTemplate = urlAnsible+url+'launch/'
            r = post(jobTemplate,auth,host_config_key)
            if r.status_code == 200 or r.status_code == 201:
                jsonRes = r.json()
                menssage = 'Execute ['+str(jsonRes['job'])+']'

    return menssage

    #print(jsonRes['results'])