import requests
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
session = requests.Session()
session.verify = False



def get(url,auth):
    r = session.get(url, auth = auth)
    return r

def post(url,auth,data):
    headers = {'Content-Type': 'application/json;'}
    r = session.post(url,headers=headers, auth=auth)
    return r

#jsonRes = r.json()
#print(r.status_code)
#print(r.text)
#print(jsonRes)