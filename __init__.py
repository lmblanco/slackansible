import os
import slack
from codigo.callAnsible import ejecutarJob

slack_token = os.environ["SLACK_API_TOKEN"]
slack_channel = os.environ["SLACK_CHANNEL"]
init_run = os.environ["INIT_RUN"]

def sendMessage(message):
    client = slack.WebClient(token=slack_token)
    client.chat_postMessage(
        channel=slack_channel,
        text=message)

@slack.RTMClient.run_on(event='message')
def say_hello(**payload):
    data = payload['data']
    #if 'run ' in data['text']:
    text = data['text']
    if text.find(init_run+' #[',0,(len(init_run)+3)) == 0:
        #print(len('run'))
        inicio = text.find("#[")
        final = text.find("]",inicio)
        #print(len(text))
        #print(text.find('run ',0,4))
        if(inicio > 0  and final >inicio):
            if inicio >0 and final > inicio+2:
                tarea = text[(inicio+2):final]
                mensaje = ejecutarJob(tarea)
                sendMessage(mensaje)
            else:
                print("")

rtm_client = slack.RTMClient(token=slack_token)
rtm_client.start()